# -*- coding: utf8 -*-

"""
Utility function file. Contains methods to convert between base 10 and base 62 numbers.
"""


def f_base10_t_base62(number, base=62, alphabet="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"):
    """
    Convert a number from base 10 (decimal) to base 62

    :param int number: base 10 number to convert
    :param int base: target base working with
    :param str alphabet: characters from base alphabet (alphanumeric for base 62)
    :return: base 62 string (str)
    """
    if number == 0:
        return alphabet[0]
    base_62_str = ""
    while number > 0:
        r = number % base
        base_62_str = alphabet[r] + base_62_str
        number = number // base
    return base_62_str


def f_base62_t_base10(number, base=62, alphabet="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"):
    """
    Convert a number from base 62 to base 10 (decimal)

    :param str number: base 62 number string to convert
    :param int base: target base working with
    :param str alphabet: characters from base alphabet (alphanumeric for base 62)
    :return: base 10 number (int)
    """
    base_10_num = 0
    digit = 0
    for s in number[::-1]:
        num = alphabet.find(s)
        base_10_num += num * (base ** digit)
        digit += 1
    return int(base_10_num)
