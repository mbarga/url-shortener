# Simple API for generating shortened URLs in Python 3
[![pipeline status](https://gitlab.com/mbarga/url-shortener/badges/master/pipeline.svg)](https://gitlab.com/mbarga/url-shortener/commits/master)

Supports multiple concurrent access requests through use of aiohttp asyncronous framework and asyncio

## Getting started
Clone the application or have the project folder present on your local workspace.

`git clone https://gitlab.com/mbarga/url-shortener.git`

There are two options for running this project:

1. Manual setup using pip/virtualenv
2. Automated setup using Docker

### Using Pip/Virtualenv

Create a virtual environment in the project folder and activate it:

```
cd ./url-shortener
```
```
virtualenv -p python3 venv
```
```
source ./venv/bin/activate
```
```
pip install -r requirements.txt
```
```
python server.py
```

### Using Docker

```
cd ./url-shortener
```
```
docker build -t url-app .
```
```
docker run -it -p 8080:8080 url-app
```

## Using API

This API requires sending of a single parameter called 'url'.

**url**: string
  * max length 200

The following endpoints are supported:

### **/encode**
POST: generates a new shortened version of the url that is passed in

Expects 'url' parameter to be passed in as json format:
```
{
  "url": "http://google.com"
}
```
```
curl -X POST 'http://0.0.0.0:8080/encode' -H "Content-Type: application/json" -d '{"url":"http://www.google.com"}'
```

### **/decode?url=http://short.ly/<code string>**
GET: redirects to the original url given a shortened url
```
curl -X GET 'http://0.0.0.0:8080/decode?url=http://short.ly/3d7'

```

## Running tests

Unit tests are built with Python standard library unittest framework.

```
python tests.py
```

I also provide a benchmarking routine to look at how long it takes to run a fixed number of asynchronous requests against the API.
The server should be running before calling this script.

```
python async_multiple_request_test.py
```

## Discussion
### Horizontal scaling
This application already supports concurrent requests through use of asyncio, however as the frequency of requests increase it would be necessary to scale the application horizontally to support more simultaneous connections. 

Computational resources for this API are relatively lightweight (decoding logic of base 10 and base 62 numbers), assuming that there are reasonable limitations on input length, etc. The biggest bottleneck is in persisted memory access. Reads and writes to the database would be very frequent in this application, so acceptable lookup times need to be achieved among shared access to persisted data.

We might also want to consider how adding more features in the future could hinder our ability to scale out. An example would be adding user sessions and the ability to save generated URL history across user sessions. User session management would require the use of caching tools to make session available in the case that the user is directed to separate application instances between different sessions.

The following are some suggestions for how to scale out this application:
1. Multiple application instances
  * One option is to use some service (through AWS, etc.) to spin up multiple instances of this application automatically depending on traffic. If more traffic is observed, a second (or nth) copy of this instance can be started. Since the logic within each instance doesnt depend on other instances, traffic can be directed through a load balancer to application instances with more resources. In this case, the database will still need to be shared between instances, so this will only help if the read/write times to the database are not the bottleneck.
2. Database sharding
  * If there is a bottleneck in database query times, or in data storage space, sharding can help. Sharding is the practice of splitting up the persisted data onto multiple machines. By splitting up data, the burden for any one machine is reduced and query times can be reduced. Of course there would also be some limited overhead in maintaining data redundancy and in defining how to shard the data.
3. Caching
  * Also related to persisted data access, caching is very commonly used in web applications. Caching can be useful to share data among application instances, and provide in memory storage to reduce fetch times. Redis or memcached are popular tools for implementing this. In this URL shortner example, it could be possible that a shortened URL for a popular web page has been shared (example on Twitter) and experiences high traffic. It would be quicker to store this data in an in memory cache for ready retrieval and less load on the persisted database.
4. Asynchronous tuning
  * There are some configurations that can be done for asyncio in terms of defining the number of workers, etc. To get better performance on a single machine, consider tuning such configurations.
5. Serverless solution
  * Since the application logic for each request in this API is decoupled from other request logic, AWS Lambda can be used as a serverless solution to process requests. This would allow for more fine grained tuning of computation resources depending on traffic and could reduce infrastructure costs. 

### Monitoring
Monitoring is also important for web applications to keep a pulse on how users are experiencing the service and track any bottlnecks in performance.

It is commonly useful to have some kind of dashboard to monitor important metrics like number of requests, response times, and error rates. Tools like Splunk can be used for this.

To quickly act on errors (HTTP 500 status), some kind of real time alerting using mail or notifications is recommended to allow engineers to quickly act on such problems. Alerts can also be setup to watch average response times. As soon as an alert is seen, an engineer would need to act to either fix the bug and deploy as soon as possible (have a redirect to a maintenance message page in the meantime), or determine the cause for long request times and (database query execution time or too many requests) and improve the code or infrastructure.

1. Logging (errors, exceptions)
  * The first thing to watch for any web application is logging. By always monitoring for 404 and 500 errors in application logs, alerts can be sent to system maintainers to quickly act when an error occurs. If the web application goes offline for any reason, this can be alerted immediately to engineers. Tools such as Splunk can be usefol for having a high level view of application performance. It is also important to structure application logs consistently using formats such as JSON or XML to make them compatible with tools such as Splunk or Sentry.
2. User requests
  * Keeping track of the average response time for user requests, and response time during peak traffic.
  * Also important to track the average number of requests per second, minute, etc.
  * Monitor periodic 'heartbeats' to the application to make sure it is online.
3. Used resources
  * Track the number of active application instances to prevent unexpected costs.
  * Track computational resources currently used (CPU usage, memory), server instances, and anything scalable.
  * Monitoring when database storage is reaching capacity and alerting with enough time to solve such issues.
4. Users and suspicious traffic
  * Keep track of what users are accessing and look for suspicious patterns (high number of requests from certain IP addresses, etc.) that could signal fraudulent activity. Consider using dynamic blacklists to block IPs, etc.

## Tools used
* aiohttp
  - lightweight asynchronous server/client for asyncio
* gitlab continuous integration pipeline

## Not implemented
* cleaner versioning (tagging, rebasing/squashing commits, etc.)
* support for https
* support for different character sets in URLs
* documentation
  - sphinx
* production deployment
  - web server/WSGI
* separate environments
  - production/staging/dev environments
* error handling
  - custom 404, etc.
* caching
  - redis/memcache to improve performance
