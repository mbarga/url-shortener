FROM python:3.6

# copy and set working directory
ADD . /url-shortener
WORKDIR /url-shortener
# install pip requirements
RUN pip install -r requirements.txt
# set environment variables
ENV PYTHONPATH=.
# application startup
ENTRYPOINT [ "python", "server.py" ]
