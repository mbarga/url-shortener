# -*- coding: UTF-8 -*-

"""
Unit testing is an important part of application development. These are not comprehensive tests, but I want
to show that I was considering unit tests in my design.
There are a number of more full featured test suites such as pytest (or aiohttp-pytest plugin) available.
Class based unit tests can be preferable sometimes, but I implemented method based tests here.
Additionally it may be preferable to implement debug logging, and toggle application logs in these tests.
"""

from aiohttp import web
from aiohttp.test_utils import TestClient
from aiohttp.test_utils import TestServer
from aiohttp.test_utils import loop_context

from server import decode_url
from server import encode_url
from utils import f_base62_t_base10
from utils import f_base10_t_base62


def test_encoder_normal():
    """test encoding under normal circumstances"""
    id = 12345
    encoded_base = f_base10_t_base62(id)
    assert encoded_base == '3d7'


def test_encoder_zero():
    """test encoding on special case of ID == 0"""
    id = 0
    encoded_base = f_base10_t_base62(id)
    assert encoded_base == '0'


def test_decoder_normal():
    """test url decoding under normal circumstances"""
    code = '3d7'
    encoded_base = f_base62_t_base10(code)
    assert encoded_base == 12345


async def test_post_encode_route_success():
    resp = await client.post("/encode", data='{"url":"http://www.google.com"}')
    assert resp.status == 200
    text = await resp.text()
    assert "3d7" in text


async def test_post_encode_route_non_json():
    resp = await client.post("/encode", data='url=http://www.google.com')
    assert resp.status == 200
    text = await resp.text()
    assert "ensure url argument in POST body is in proper json format" in text


async def test_get_decode_route_success():
    resp = await client.get("/decode?url=http://short.ly/3d7", allow_redirects=False)
    assert resp.status == 302


async def test_get_decode_route_not_exists():
    resp = await client.get("/decode?url=http://short.ly/333")
    assert resp.status == 200
    text = await resp.text()
    assert "requested lookup URL is not present in database" in text


# TODO: more tests to achieve better code coverage...


if __name__ == "__main__":
    # test static decoder/encoder functions
    test_encoder_normal()
    test_encoder_zero()
    test_decoder_normal()

    # test asynchronous client calls to application
    with loop_context() as loop:
        # setup application context and event loop
        app = web.Application()
        app.add_routes([web.get('/decode', decode_url),
                        web.post('/encode', encode_url)])
        client = TestClient(TestServer(app), loop=loop)
        loop.run_until_complete(client.start_server())

        # start calling the API with test methods
        loop.run_until_complete(test_post_encode_route_success())
        loop.run_until_complete(test_post_encode_route_non_json())
        loop.run_until_complete(test_get_decode_route_success())
        loop.run_until_complete(test_get_decode_route_not_exists())

        # destroy the test client
        loop.run_until_complete(client.close())

    print("#############\nFINISHED SUCCESSFULLY\n#############")
