# -*- coding: utf8 -*-

"""
This file is the main application. It contains the event loop startup code, as well as the URL endpoint handlers.
"""

import asyncio
import logging
from json import JSONDecodeError

from aiohttp import web

from utils import f_base10_t_base62
from utils import f_base62_t_base10

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


"""Here, 'database' is a global variable python dictionary that is serving as a replacement for persisted data storage.
Instead of this variable, we could use a database client (such as MongoDB):
    from pymongo import MongoClient
    c = MongoClient()
    db = c.database
For a URL shortening service, it would also be useful to use a caching tool (key-value store like Redis) to retrieve 
frequently accessed  URL lookups to improve performance.

The variable 'id_counter' is serving as an auto-incremented key to the dictionary. If we used a database client,
the database itself commonly handles the ID generation (for example GUID or mongodb _id field) for newly inserted data.
"""
id_counter = 12345
database = {}

"""Asyncio supports locking constructs. Locks are normally seen in multithreaded applications, but asyncio coroutines also
behave similarly because a coroutine can still yield to another coroutine during execution.
This is important because for example, if we have two database insertions happening at the same time, it could be possible 
that one asynchronous process updates the key before another process has the chance to write its data to the database. This
would cause incorrect data to be written to the database (same key used for two writes would cause the former to be overwritten).
For this reason, we want to make sure that any update to the key happens atomically with the write to the database (as done
in the application code below where the lock is used). If we were using a persistent database store, we would want to ensure
that all writes happen atomically as well. 
"""
_lock = asyncio.Lock()


def increment_id():
    """
    increment database key by 1
    """
    global id_counter
    id_counter += 1


# POST
async def encode_url(request):
    """
    Take in a url string in parameters and pass back a shortened URL.
    """
    global _lock
    try:
        body_data = await request.json()
        logger.info("found parameters: " + str(body_data))

        url_param = body_data.get('url')
        if url_param is None:
            logger.error('"url" parameter is required, but not present in request body')
            return web.Response(
                text='{"status": "error", "message": "required url parameter was not present in request"}'
            )
        if len(url_param) > 200:
            logger.error('"url" to shorten is too long (must be less than 200 characters)')
            return web.Response(
                text='{"status": "error", "message": "url is too long (must be less than 200 characters"}'
            )

        logger.info("encoding original URL: " + url_param)

        ####### must be atomic ############
        async with _lock:
            """
            persist to database with:
                id = db.collection.insert({id_counter: url_param})
            """
            database.update({id_counter: url_param})
            base62_converted_stub = f_base10_t_base62(id_counter)
            increment_id()
            await asyncio.sleep(0)
        ####### end atomic ################

        # introduce artificial delay for testing
        delay_param = body_data.get('delay_sec')
        if delay_param is not None:
            try:
                delay = int(delay_param)
                await asyncio.sleep(delay)
            except:
                pass

        return_url = "http://short.ly/" + str(base62_converted_stub)
        return web.Response(text=return_url)
    except JSONDecodeError as e:
        logger.error("error decoding JSON format parameters from POST body")
        return web.Response(
            text='{"status": "error", "message": "ensure url argument in POST body is in proper json format"}'
        )
    except Exception as e:
        logger.error("{0}({1})".format(type(e).__name__, str(e)))
        return web.Response(
            text='{"status": "error", "message": "could not generate a URL"}'
        )


# GET
async def decode_url(request):
    """
    Redirect to a decoded URL given a shortened URL passed in parameters.
    If shortened URL doesnt exist in database, then pass back a message stating such.
    """
    try:
        params = request.rel_url.query
        logger.info("found parameters: " + str(params))

        url_param = params.get('url')
        if url_param is None:
            logger.error('"url" parameter is required, but not present in request parameters')
            return web.Response(
                text='{"status": "error", "message": "required url parameter was not present in request"}'
            )

        logger.info("decoding original URL: " + url_param)

        code = url_param.replace("http://short.ly/", "")
        base10_converted_stub = f_base62_t_base10(code)
        logger.info("converted url parameter to key value: " + str(base10_converted_stub))
        lookup = database.get(base10_converted_stub)
        if lookup is None:
            logger.info('requested lookup URL is not present in database')
            return web.Response(
                text='{"status": "success", "message": "requested lookup URL is not present in database"}'
            )
        else:
            logger.info("forwarding to URL: " + lookup)
            return web.HTTPFound(lookup)
    except Exception as e:
        logger.error("{0}({1})".format(type(e).__name__, str(e)))
        return web.Response(
            text='{"status": "error", "message": "could not decode the URL"}'
        )


async def init(loop):
    """
    Initialize server given asyncio event loop.

    :param loop: asyncio event loop
    """
    app = web.Application()
    app.add_routes([web.get('/decode', decode_url),
                    web.post('/encode', encode_url)])
    logger.info("======= Serving on http://0.0.0.0:8080/ ======")
    return await loop.create_server(
        app.make_handler(), '0.0.0.0', 8080
    )


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(init(loop))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    loop.close()
