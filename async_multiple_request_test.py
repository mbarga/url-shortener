# -*- coding: utf8 -*-

"""
Supplementary routine to test asynchronous requests to the server and look at performance.
"""

import asyncio
import logging
import time
from random import randint

from aiohttp import ClientSession

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


async def fetch(url, session, delay):
    """
    Make an asynchronous request to server endpoint.

    :param url: API endpoint
    :param session: test client session
    :param delay: artificial delay to inject to request
    """
    param_json = """{{"url":"http://www.google.com/{0}", "delay_sec": "{1}"}}""".format(str(randint(1, 100)),
                                                                                        str(delay))
    async with session.post(url, data=param_json) as resp:
        return await resp.read()


async def run(num_requests, delay_seconds):
    """
    Create an asynchronous test client and make a fixed number of requests to the API server.

    :param num_requests: total number of test requests to make to server
    :param delay_seconds: seconds of artificial delay to inject into request
    """
    url = "http://0.0.0.0:8080/encode"
    tasks = []

    async with ClientSession() as session:
        for i in range(num_requests):
            task = asyncio.ensure_future(fetch(url, session, delay_seconds))
            tasks.append(task)

        for resp in await asyncio.gather(*tasks):
            # logger.info("response: {0}".format(resp))
            pass


if __name__ == "__main__":
    delay = 5
    loop = asyncio.get_event_loop()

    """
    Here I inject an artificial delay of 5 seconds to each request. For 20-100 requests, it takes around 5 seconds to
    process all of the requests on the server, which is the same amount of time that it takes to process a single request
    (with a slight increase due to overhead). This shows that the server properly handles asynchronous requests.
    """
    # 20 requests
    number_requests = 20
    logger.info("Running {} requests with individual request delay of {}s...".format(number_requests, str(delay)))
    future = asyncio.ensure_future(run(number_requests, delay))
    start_time = time.time()
    loop.run_until_complete(future)
    elapsed_time = time.time() - start_time
    logger.info("Total elapsed time was: {0:.2f}s".format(elapsed_time))

    # 100 requests
    number_requests = 100
    logger.info("Running {} requests with request delay of {}s...".format(number_requests, str(delay)))
    future = asyncio.ensure_future(run(number_requests, delay))
    start_time = time.time()
    loop.run_until_complete(future)
    elapsed_time = time.time() - start_time
    logger.info("Total elapsed time was: {0:.2f}s".format(elapsed_time))

    # 1000 requests
    """
    For 1000 requests, my machine slows down significantly (around 50s to process all of the requests). There would need
    to be more performance tuning to fix this. For example, the number of workers could be tuned depending on the number
    of processing cores available in the machine running the code, or perhaps there are bottlenecks in the application code
    in the use of locks, etc.
    """
    number_requests = 1000
    logger.info("Running {} requests with request delay of {}s...".format(number_requests, str(delay)))
    future = asyncio.ensure_future(run(number_requests, delay))
    start_time = time.time()
    loop.run_until_complete(future)
    elapsed_time = time.time() - start_time
    logger.info("Total elapsed time was: {0:.2f}s".format(elapsed_time))
